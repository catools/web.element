package org.catools.web.tests.visual;

import org.catools.web.enums.CChromeEmulatedDevice;
import org.testng.annotations.BeforeClass;

public class IPadChromeVisualTest extends VisualTest {

    @BeforeClass(alwaysRun = true)
    @Override
    public void beforeClass() {
        super.beforeClass();
        switchToChrome().setDeviceEmulation(CChromeEmulatedDevice.IPAD.getDeviceName()).setHeadless(true);
        open(BASE_URL);
    }

}

package org.catools.web.tests.visual;

import org.openqa.selenium.Dimension;
import org.testng.annotations.BeforeClass;

public class DesktopChromeVisualTest extends VisualTest {

    @BeforeClass(alwaysRun = true)
    @Override
    public void beforeClass() {
        super.beforeClass();
        switchToChrome();
        getDriverSession().setWindowsSize(new Dimension(1024, 1366));
        open(BASE_URL);
    }
}

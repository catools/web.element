package org.catools.web.tests.visual;

import org.catools.common.collections.CList;
import org.catools.common.io.CResource;
import org.catools.common.tests.CTest;
import org.catools.media.utils.CImageUtil;
import org.catools.web.tests.CDriverTest;
import org.testng.annotations.Test;

import java.awt.image.BufferedImage;

public abstract class VisualTest extends CDriverTest {

    @Test
    public void dashboard() {
        getDriver().ScreenShot.verifyEquals(this, new CResource("images/expected/" + getName() + "_" + "petclinic.png", CTest.class), "Screen shot match");
    }

    @Test
    public void dashboardAny() {
        BufferedImage bf1 = CImageUtil.readImageOrNull(new CResource(getName() + "Invalid", CTest.class));
        String resourceFullName = getName() + "_" + "petclinic.png";
        BufferedImage bf2 = CImageUtil.readImageOrNull(new CResource("images/expected/" + resourceFullName, CTest.class));
        getDriver().ScreenShot.verifyEqualsAny(this, (Iterable) new CList(bf1, bf2), resourceFullName, 10, "Screen shot match");
    }
}

package org.catools.web.listeners;

import org.catools.web.tests.CWebTest;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class CIInvokedMethodListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (testResult.getStatus() != ITestResult.SUCCESS && testResult.getInstance() instanceof CWebTest) {
            CWebTest testInstance = (CWebTest) testResult.getInstance();
            if (testInstance.isCurrentSessionActive()) {
                testInstance.takeScreenShotIfFail(testResult);
            }
        }
    }
}